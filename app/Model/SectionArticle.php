<?php
App::uses('AppModel', 'Model');

class SectionArticle extends AppModel {
	var $name = 'SectionArticle';
	var $displayField = 'title';
	var $actsAs = array('Containable');
	var $order = 'SectionArticle.created ASC';
	// Relations
	var $belongsTo = array('Section');
	
	var $validate = array(
		'section_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This is a required field and cannot be left empty.'
		),
		'title' => array(
			'rule' => 'notEmpty',
			'message' => 'This is a required field and cannot be left empty.'
		),
		'content' => array(
			'rule' => 'notEmpty',
			'message' => 'This is a required field and cannot be left empty.'
		)
	);
}