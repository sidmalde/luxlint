<?php

	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
	Router::connect('/luxury-hotels', array('controller' => 'pages', 'action' => 'hotels'));
	Router::connect('/luxury-apartments', array('controller' => 'pages', 'action' => 'apartments'));
	Router::connect('/luxury-cars', array('controller' => 'pages', 'action' => 'cars'));
	Router::connect('/private-jet-helicopter', array('controller' => 'pages', 'action' => 'private_jet'));
	Router::connect('/hospitality', array('controller' => 'pages', 'action' => 'hospitality'));
	Router::connect('/security-services', array('controller' => 'pages', 'action' => 'security'));
	
	Router::connect('/admin/dashboard', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	
	// Groups
	Router::connect('/system-management/groups', array('controller' => 'groups', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/groups/new', array('controller' => 'groups', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/groups/:group', array('controller' => 'groups', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/groups/:group/edit', array('controller' => 'groups', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/groups/:group/delete', array('controller' => 'groups', 'action' => 'delete', 'admin' => true));
	
	// Users
	Router::connect('/system-management/users', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	Router::connect('/system-management/users/new', array('controller' => 'users', 'action' => 'add', 'admin' => true));
	Router::connect('/system-management/users/:user', array('controller' => 'users', 'action' => 'view', 'admin' => true));
	Router::connect('/system-management/users/:user/edit', array('controller' => 'users', 'action' => 'edit', 'admin' => true));
	Router::connect('/system-management/users/:user/delete', array('controller' => 'users', 'action' => 'delete', 'admin' => true));
	
	/*** CONTENT ***/
	
	// Sections
	Router::connect('/content-management/sections', array('controller' => 'sections', 'action' => 'index', 'admin' => true));
	Router::connect('/content-management/sections/new', array('controller' => 'sections', 'action' => 'add', 'admin' => true));
	Router::connect('/content-management/sections/:section', array('controller' => 'sections', 'action' => 'view', 'admin' => true));
	Router::connect('/content-management/sections/:section/edit', array('controller' => 'sections', 'action' => 'edit', 'admin' => true));
	Router::connect('/content-management/sections/:section/delete', array('controller' => 'sections', 'action' => 'delete', 'admin' => true));
	
	// Section Articles
	Router::connect('/content-management/section-articles', array('controller' => 'section_articles', 'action' => 'index', 'admin' => true));
	Router::connect('/content-management/section-articles/new', array('controller' => 'section_articles', 'action' => 'add', 'admin' => true));
	Router::connect('/content-management/section-articles/:sectionArticle', array('controller' => 'section_articles', 'action' => 'view', 'admin' => true));
	Router::connect('/content-management/section-articles/:sectionArticle/edit', array('controller' => 'section_articles', 'action' => 'edit', 'admin' => true));
	Router::connect('/content-management/section-articles/:sectionArticle/delete', array('controller' => 'section_articles', 'action' => 'delete', 'admin' => true));
	
	// Images
	Router::connect('/content-management/uploads', array('controller' => 'uploads', 'action' => 'index', 'admin' => true));
	Router::connect('/content-management/uploads/new', array('controller' => 'uploads', 'action' => 'add', 'admin' => true));
	Router::connect('/content-management/uploads/:upload', array('controller' => 'uploads', 'action' => 'view', 'admin' => true));
	Router::connect('/content-management/uploads/:upload/edit', array('controller' => 'uploads', 'action' => 'edit', 'admin' => true));
	Router::connect('/content-management/uploads/:upload/delete', array('controller' => 'uploads', 'action' => 'delete', 'admin' => true));
	
	// Product Types
	Router::connect('/content-management/product-types', array('controller' => 'product_types', 'action' => 'index', 'admin' => true));
	Router::connect('/content-management/product-types/new', array('controller' => 'product_types', 'action' => 'add', 'admin' => true));
	Router::connect('/content-management/product-types/:productType', array('controller' => 'product_types', 'action' => 'view', 'admin' => true));
	Router::connect('/content-management/product-types/:productType/edit', array('controller' => 'product_types', 'action' => 'edit', 'admin' => true));
	Router::connect('/content-management/product-types/:productType/delete', array('controller' => 'product_types', 'action' => 'delete', 'admin' => true));
	
	// Products
	Router::connect('/content-management/products', array('controller' => 'products', 'action' => 'index', 'admin' => true));
	Router::connect('/content-management/products/new', array('controller' => 'products', 'action' => 'add', 'admin' => true));
	Router::connect('/content-management/products/:product', array('controller' => 'products', 'action' => 'view', 'admin' => true));
	Router::connect('/content-management/products/:product/edit', array('controller' => 'products', 'action' => 'edit', 'admin' => true));
	Router::connect('/content-management/products/:product/delete', array('controller' => 'products', 'action' => 'delete', 'admin' => true));
	
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
