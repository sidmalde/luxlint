<div class="row">
	<div class="col-md-12">
		<h1 class="text-center"><?=__('Luxury Apartments');?></h1><br />
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Knightsbridge');?></h4><hr />
			</div>
			<div class="col-md-5">
				<p>
					Knightsbridge is a vibrant shopping district, home to Harrods, 
					Harvey Nichols and Sloan Street a world famous street for boutique 
					designer shops. It has been identified as one of two international 
					centres in London. 
				</p>
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Apartments/Knightsbridge/thumb-01.jpg" class="thumbnail"/>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Mayfair');?></h4><hr />
			</div>
			<div class="col-md-5">
				This area has become a busy hub for business, shopping and the luxury 
				lifestyle, with many corporate HQs, embassies, exclusive shops and the 
				largest collection of luxury hotels & restaurants.
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Apartments/Mayfair/thumb-02.jpg" class="thumbnail"/>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Riverside');?></h4><hr />
			</div>
			<div class="col-md-5">
				Our riverside apartments have stunning views of the cityscape & the 
				River Thames and the night time views are breath-taking. 
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Apartments/Knightsbridge/thumb-03.jpg" class="thumbnail"/>
			</div>
		</div>
	</div>
</div>