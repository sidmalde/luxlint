<?
$cars = array(
	'Prestige' =>  array(
		'Bentley Flying Spur' => '/img/banner/Cars/Bentley/Flying Spur/thumb-01.jpg',
		'Rolls Royce Ghost' => '/img/banner/Cars/Rolls Royce/Ghost/thumb-01.jpg',
		'Bentley Mulsanne' => '/img/banner/Cars/Bentley/Mulsanne/thumb-01.jpg',
	),
	'Luxury' =>  array(
		'Mercedes S500' => '/img/banner/Cars/Mercedes/S500/thumb-01.jpg',
		'Range Rover Vogue' => '/img/banner/Cars/Range Rover/Vogue/thumb-01.jpg',
		'BMW 7-Series' => '/img/banner/Cars/BMW/7-Series/thumb-01.jpg',
	),
	'Executive' =>  array(
		'Mercedes E-Class' => '/img/banner/Cars/Mercedes/E350/thumb-01.jpg',
		'BMW 5-Series' => '/img/banner/Cars/BMW/5-Series/thumb-01.jpg',
		'Mercedes Viano' => '/img/banner/Cars/Mercedes/Viano/thumb-02.jpg',
	),
	);
?>
<div class="row">
	<div class="col-md-12">
		<h1 class="text-center"><?=__('Luxury Cars');?></h1>
	</div>
	<div class="col-md-12">
		<h3><?=__('Prestige Cars');?></h3><hr />
	</div>
<? foreach($cars['Prestige'] as $car => $thumb): ?>
	<div class="col-sm-4 divider box-link">
		<a href="#">
			<img src="<?=$thumb;?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=$car;?></span>
		</a>
	</div>
<? endforeach; ?>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<h3><?=__('Luxury Cars');?></h3><hr />
	</div>
	<? foreach($cars['Luxury'] as $car => $thumb): ?>
		<div class="col-sm-4 divider box-link">
			<a href="#">
				<img src="<?=$thumb;?>" class="thumbnail" />
				<br />
				<span class="btn btn-default btn-sm btn-block"><?=$car;?></span>
			</a>
		</div>
	<? endforeach; ?>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<h3><?=__('Executive Cars');?></h3><hr />
	</div>
	<? foreach($cars['Executive'] as $car => $thumb): ?>
		<div class="col-sm-4 divider box-link">
			<a href="#">
				<img src="<?=$thumb;?>" class="thumbnail" />
				<br />
				<span class="btn btn-default btn-sm btn-block"><?=$car;?></span>
			</a>
		</div>
	<? endforeach; ?>
</div>