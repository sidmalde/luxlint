<div class="row">
	<div class="col-sm-12">
		<h1 class="text-center"><?=__('Luxury & Boutique Hotels');?></h1>
	</div>

<?
$hotels = array(
	'The Dorchester' => '/img/banner/Hotels/The Dorchester/lobby-thumb.jpg',
	'Mandarin' => '/img/banner/Hotels/Mandarin/facade-thumb.jpg',
	'Bvlgari' => '/img/banner/Hotels/Bvlgari/facade-thumb.jpg',
	'45 Park Lane' => '/img/banner/Hotels/45 Park Lane/suite-thumb.jpg',
	'Landmark' => '/img/banner/Hotels/The Landmark/landmark-thumb.jpg',
	'Corinthia' => '/img/banner/Hotels/The Corinthia/suite-thumb.jpg',
	/* 'Browns' => '', */
);
?>
<? $i=1;foreach($hotels as $hotel => $thumb): ?>
	<? if($i == 1): ?>
		<div class="row">
	<? endif; ?>
		<div class="col-sm-4 divider">
			<a href="#" class="box-link">
				<img src="<?=$thumb;?>" class="thumbnail" />
				<br />
				<span class="btn btn-default btn-sm btn-block"><?=$hotel;?></span>
			</a>
		</div>
	<? if($i == 3): ?>
		</div><br />
	<? $i=0;endif; ?>
<? $i++;endforeach; ?>
</div>