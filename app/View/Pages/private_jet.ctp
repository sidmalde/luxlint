<div class="row">
	<div class="col-md-12">
		<h1 class="text-center"><?=__('Private Jet & Helicopter');?></h1><br />
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Private Jet');?></h4><hr />
			</div>
			<div class="col-md-5">
				<p>
					We have access to Private Jets available to hire on demand for your 
					personal / business uses. Comfortable, luxurious and fully catered 
					jets await you. With on-board entertainment and refreshments you can 
					use the jets for business or pleasure. We ensure all your requirements 
					are met, with regards to food, drinks and entertainment. 
				</p>
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Private Jet/thumb-01.jpg" class="thumbnail"/>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Helicopter');?></h4><hr />
			</div>
			<div class="col-md-5">
				Is time of the essence? We can procure readily available helicopters to fly 
				you from A to B. These helicopters are fuelled and ready to go at a moment’s 
				notice, to transport you to important and time sensitive meetings. Enjoy the 
				freedom of traffic beating transport.
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Private Jet/thumb-02.jpg" class="thumbnail"/>
			</div>
		</div>
	</div>
</div>