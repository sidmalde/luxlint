<div class="row">
	<div class="col-md-12">
		<h1 class="text-center"><?=__('Security Services');?></h1><hr />
		<div class="row">
			<div class="col-md-5">
				<p>
					We can arrange for security individuals or teams for a wide range of 
					situations including public & social events, high profile & business 
					meetings and even day to day protection.
				</p>
				<p>
					Each security personnel has been vetted and selected from only the 
					best the industry has to offer.
				</p>
				<p>
					We understand the high profile of our clients and respectively we 
					offer tailor made security packages for our client’s individual needs. 
					Our expertise and knowledge in the security field are paramount which 
					is why we offer such a high level of service and client satisfaction. 
				</p>
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Security Services/thumb-01.jpg" class="thumbnail"/>
			</div>
		</div>
	</div>
</div>