<div class="row">
	<div class="col-md-12"><h1 class="text-center"><?=__('Hospitality');?></h1><br /></div>
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Sporting Events');?></h4><hr />
			</div>
			<div class="col-md-5">
				<p>
					We pride ourselves on being able to obtain tickets for all major sporting 
					events. We have access to Champions League & Premier League matches as 
					well as international matches. We are also able to obtain tickets for 
					Formula-1 & Wimbledon.
				</p>
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Hospitality/thumb-01.jpg" class="thumbnail"/>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<h4><?=__('Ticket Reservations');?></h4><hr />
			</div>
			<div class="col-md-5">
				We have an excellent team capable of getting Theatre, Concert and Private Events
				tickets / reservations. Even booking dining at world-renowned restaurants another 
				of the services amongst our repertoire. We have the expertise and relationships 
				with London's finest venues to ensure you have no issues gaining access to your 
				selected events.
			</div>
			<div class="col-md-5 col-md-offset-2 divider box-link">
				<img src="/img/banner/Hospitality/thumb-03.jpg" class="thumbnail"/>
			</div>
		</div>
	</div>
</div>