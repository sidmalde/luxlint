<?
	$hotels = array(
		0 => '/img/banner/Hotels/The Dorchester/lobby-thumb.jpg',
		1 => '/img/banner/Hotels/Mandarin/facade-thumb.jpg',
		2 => '/img/banner/Hotels/Bvlgari/facade-thumb.jpg',
		3 => '/img/banner/Hotels/45 Park Lane/suite-thumb.jpg',
		4 => '/img/banner/Hotels/The Landmark/landmark-thumb.jpg',
		5 => '/img/banner/Hotels/The Corinthia/suite-thumb.jpg',
	);
	$apartments = array(
		0 => '/img/banner/Apartments/apartment-thumb-01.jpg',
		1 => '/img/banner/Apartments/apartment-thumb-02.jpg',
		2 => '/img/banner/Apartments/apartment-thumb-03.jpg',
		3 => '/img/banner/Apartments/apartment-thumb-04.jpg',
	);
	$cars = array(
		0 => '/img/banner/Cars/Mercedes/S500/thumb-01.jpg',
		1 => '/img/banner/Cars/BMW/5-Series/thumb-01.jpg',
		2 => '/img/banner/Cars/Bentley/Flying Spur/thumb-01.jpg',
		3 => '/img/banner/Cars/Bentley/Mulsanne/thumb-01.jpg',
		4 => '/img/banner/Cars/Rolls Royce/Ghost/thumb-01.jpg',
		5 => '/img/banner/Cars/Rolls Royce/Wraith/thumb-01.jpg',
	);
	$hospitality = array(
		0 => '/img/banner/Hospitality/thumb-01.jpg',
		1 => '/img/banner/Hospitality/thumb-02.jpg',
		2 => '/img/banner/Hospitality/thumb-03.jpg',
	);
	$privateJet = array(
		0 => '/img/banner/Private Jet/thumb-01.jpg',
		1 => '/img/banner/Private Jet/thumb-02.jpg',
	);
	$securityServices = array(
		0 => '/img/banner/Security Services/thumb-01.jpg',
		1 => '/img/banner/Security Services/thumb-02.jpg',
	);
?>
<div class="row">
	<div class="col-sm-4 divider left-divider">	
		<a href="/luxury-hotels" class="box-link">
			<img src="<?=$hotels[rand(0,5)]?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=__('Luxury & Boutique Hotels');?></span>
		</a>
	</div>
	<div class="col-sm-4 divider">
		<a href="/luxury-apartments" class="box-link">
			<img src="<?=$apartments[rand(0,3)]?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=__('Luxury Apartments');?></span>
		</a>
	</div>
	<div class="col-sm-4 divider">
		<a href="/luxury-cars" class="box-link">
			<img src="<?=$cars[rand(0,5)]?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=__('Luxury Cars');?></span>
		</a>
	</div>
</div>
<br />
<div class="row">
	<div class="col-sm-4 divider">
		<a href="/hospitality" class="box-link">
			<img src="<?=$hospitality[rand(0,2)]?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=__('Hospitality');?></span>
		</a>
	</div>
	<div class="col-sm-4 divider">
		<a href="/private-jet-helicopter" class="box-link">
			<img src="<?=$privateJet[rand(0,1)]?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=__('Private Jet &amp; Helicopter');?></span>
		</a>
	</div>
	<div class="col-sm-4 divider">
		<a href="/security-services" class="box-link">
			<img src="<?=$securityServices[rand(0,1)]?>" class="thumbnail" />
			<br />
			<span class="btn btn-default btn-sm btn-block"><?=__('Security Services');?></span>
		</a>
	</div>
</div>