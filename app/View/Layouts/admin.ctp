<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset(); ?>
	<title>
		<?=$title_for_layout; ?>
	</title>
	<?=$this->Html->meta('icon');?>

	<?=$this->fetch('meta');?>
	<?=$this->fetch('script');?>
	
	<?=$this->Html->script('jquery-1.10.2.min'); ?>
	
	<?=$this->Html->css('font-awesome/css/font-awesome.min'); ?>
	<?=$this->Html->css('bootstrap.min'); ?>
	<?=$this->Html->css('slate_bootstrap');?>
	<?=$this->Html->css('core'); ?>
	<?=$this->Html->css('jquery.Jcrop'); ?>
	
	<?=$this->Html->script('bootstrap.min'); ?>
	<?=$this->Html->script('core'); ?>
	<?=$this->Html->script('jquery.Jcrop'); ?>
</head>
<body class="admin">
	<?=$this->element('header-admin');?>
	<div id="container" class="container">
		<div id="content" class="row">
			<div class="col-xs-12">
				<?=$this->Session->flash(); ?>

				<?=$this->fetch('content'); ?>
			</div>
		</div>
		<div id="footer" class="row">
			<div class="col-xs-12">
			</div>
		</div>
		<div class="clear">
		</div>
	</div>
</body>
</html>
