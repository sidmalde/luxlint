<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset(); ?>
	<title>
		<?=$title_for_layout; ?>
	</title>
	<?=$this->Html->meta('icon');?>

	<?=$this->fetch('meta');?>
	<?=$this->fetch('script');?>
	
	<?=$this->Html->script('jquery-1.10.2.min'); ?>
	
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<?=$this->Html->css('bootstrap.min'); ?>
	<?=$this->Html->css('slate_bootstrap');?>
	<?=$this->Html->css('core'); ?>
	<?=$this->Html->css('nivo'); ?>
	<?=$this->Html->css('nivo-slider/bar/bar'); ?>
	<?=$this->Html->css('nivo-slider/dark/dark'); ?>
	<?=$this->Html->css('nivo-slider/light/light'); ?>
	<?=$this->Html->css('nivo-slider/default/default'); ?>
	
	<?=$this->Html->script('bootstrap.min'); ?>
	<?=$this->Html->script('nivo'); ?>
	<?=$this->Html->script('core'); ?>
</head>
<body>
	<?=$this->element('header-default');?>
	<? if ($this->request->url == false) { echo $this->element('header-banner');} ?>
	<div id="container" class="container">
		<div id="content" class="row">
			<div class="col-xs-12">
				<?#=$this->Session->flash(); ?>

				<div class="content-container">
					<?=$this->fetch('content'); ?>
				</div>
			</div>
		</div>
		<div id="footer" class="row">
			<div class="col-xs-12">
			</div>
		</div>
		<div class="clear">
		</div>
	</div>
	<footer>
		<?=$this->element('footer-default');?>
	</footer>
	<?=$this->element('flash_container');?>
</body>
</html>
