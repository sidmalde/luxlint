<p class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<span class="title"><span class="label label-important"><i class=" icon-ban-circle"></i> <?=__('Error')?></span></span>
	<?=$message;?>
</p>