<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header-social-row">
					<a href="https://www.facebook.com/pages/Luxury-lifestyle-international/113839408789580" target="_blank"><i class="fa fa-facebook-square fa-5"></i></a>
					<a href="https://twitter.com/luxlint" target="_blank"><i class="fa fa-twitter-square fa-5"></i></a>
					<a href="#"><i class="fa fa-google-plus-square fa-5"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<?/*<ul class="footer-links letter-space-xs text-md">
					<li><?=$this->Html->link('<i class="fa fa-dot-circle-o"></i> '.__('Luxury & Boutique Hotels'), '/luxury-hotels', array('escape' => false));?></li>
					<li><?=$this->Html->link('<i class="fa fa-dot-circle-o"></i> '.__('Luxury Apartments'), '/luxury-apartments', array('escape' => false));?></li>
					<li><?=$this->Html->link('<i class="fa fa-dot-circle-o"></i> '.__('Luxury Cars'), '/luxury-cars', array('escape' => false));?></li>
					<li><?=$this->Html->link('<i class="fa fa-dot-circle-o"></i> '.__('Hospitality'), '/luxury-cars', array('escape' => false));?></li>
					<li><?=$this->Html->link('<i class="fa fa-dot-circle-o"></i> '.__('Private Jet & Helicopter'), '/private-jet-helicopter', array('escape' => false));?></li>
					<li><?=$this->Html->link('<i class="fa fa-dot-circle-o"></i> '.__('Security Services'), '/security-services', array('escape' => false));?></li>
				</ul>*/?>
				   <?=$this->Html->link(__('Luxury & Boutique Hotels'), '/luxury-hotels', array('escape' => false));?> 
				 | <?=$this->Html->link(__('Luxury Apartments'), '/luxury-hotels', array('escape' => false));?>
				 | <?=$this->Html->link(__('Luxury Cars'), '/luxury-cars', array('escape' => false));?>
				 | <?=$this->Html->link(__('Hospitality'), '/luxury-cars', array('escape' => false));?>
				 | <?=$this->Html->link(__('Private Jet & Helicopter'), '/private-jet-helicopter', array('escape' => false));?>
				 | <?=$this->Html->link(__('Security Services'), '/security-services', array('escape' => false));?>
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-sm-12">
				<p class="text-center text-muted">
					Luxury Lifestyle International is a trading name Of Eshall Ltd. Registered in England & Wales No. 0693780.
				</p>
				<p class="text-center text-muted">
					9 Lewis House, School Road, London, NW10 6TD, UK
				</p>
				<p class="text-center text-muted letter-space-xs">
					<a href="callto:<?=__('+442079938865');?>" class="text-muted"><i class="fa fa-phone blue-text"></i> 020 7993 8865</a> | 
					<i class="fa fa-print blue-text"></i> 020 8838 3115 | 
					<a href="mailto:info@luxlint.com" class="text-muted"><i class="fa fa-envelope blue-text"></i> info@luxlint.com</a>
				</p>
			</div>
		</div>
	</div>
	<?/*<div class="col-sm-6">
		<p class="pull-right"><?=__('Social');?></p>
	</div>*/?>
</div>