<div class="container">
	<div class="main-header row">
		<div class="col-md-4">
			<?=$this->Html->image('/img/logos/logo-web.png', array('url' => '/'));?>
		</div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-4 pull-right">
					<div class="text-left">
						<h3><a href="callto:<?=__('+442079938865');?>" class="letter-space-xs"><i class="fa fa-phone blue-text"></i> <?=__('020 7993 8865');?></a></h3>
						<h3><a href="mailto:info@luxlint.com"><i class="fa fa-envelope blue-text"></i> info@luxlint.com</h3>
					</div>
				</div>
			</div>
			<?/*<div class="">
				<?=$this->Html->link(__('Contact Us'), '#', array('class' => 'btn btn-default btn-block btn-sm'));?>
			</div>*/?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="navbar navbar-default">
				  <div class="container">
					<div class="navbar-header">
						<a href="/" class="navbar-brand"><i class="fa fa-home"></i></a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<ul class="nav navbar-nav">
							<li><a href="/luxury-hotels"><?=__('Hotels');?></a></li>
							<li><a href="/luxury-apartments"><?=__('Apartments');?></a></li>
							<li><a href="/luxury-cars"><?=__('Cars');?></a></li>
							<li><a href="/hospitality"><?=__('Hospitality');?></a></li>
							<li><a href="/private-jet-helicopter"><?=__('Private Jet &amp; Helicopter');?></a></li>
							<li><a href="/security-services"><?=__('Security Services');?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>