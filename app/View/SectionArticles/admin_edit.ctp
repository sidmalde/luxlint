<?=$this->Form->create('SectionArticle'); ?>
	
	<?=$this->Form->input('id');?>
	<?=$this->Form->input('section_id', array('empty' => __('Please select an option:'), 'options' => $sections));?>
	<?=$this->Form->input('title', array('placeholder' => 'Title'));?>
	<?=$this->Form->input('content', array('type' => 'textarea', 'placeholder' => 'Content'));?>
	<?=$this->Form->input('position', array('placeholder' => 'Position'));?>
   	
	<div class="btn-group">
      <button class="btn btn-success" type="submit" data-loading-text="Saving...">Save</button>
      <?=$this->Html->link('Cancel', array('action' => 'index'), array('class' => 'btn btn-danger'), 'Are you sure you want to cancel?'); ?>		</div>
	</fieldset>
<?=$this->Form->end(); ?>
