<h3>
	<?=@$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add'), array('class' => 'btn btn-primary btn-sm'));?>
	</div>
</h3>

<div class="row">
	<div class="col-xs-12">
		<? if (!empty($uploads)): ?>
			<table class="table table-condensed table-striped table-bordered table-hover">
				<tr>
					<th><?=__('Label');?></th>
					<th><?=__('Original');?></th>
					<th><?=__('L');?></th>
					<th><?=__('M');?></th>
					<th><?=__('S');?></th>
					<th><?=__('T');?></th>
					<th><?=__('Created');?></th>
				</tr>
				<? foreach ($uploads as $upload): ?>
					<tr>
						<td><?=$upload['Group']['name'];?></td>
						<td><?=$this->Time->niceDate($upload['Upload']['created']);?></td>
						<td class="actions">
							<div class="btn-group">
								<?=$this->Html->link(__('Actions'), '#', array('class' => 'btn btn-info'));?>
								<ul class="dropdown-menu">
									<?=$this->Html->link(__('Edit').'<span class="caret"></span>', array('controller' => 'uploads', 'action' => 'edit', 'upload' => $upload['Upload']['id']), array('escape' => false, 'class' => 'btn btn-success dropdown-toggle', 'data-toggle' => 'dropdown'));?>
									<li><?=$this->Html->link(__('Delete'), array('controller' => 'uploads', 'action' => 'delete', 'upload' => $upload['Upload']['id']));?></li>
								</ul>
							</div>
						</td>
					</tr>
				<? endforeach; ?>
			</table>
		<? else: ?>
			<div class="alert alert-info">
				<p class="text-center lead"><strong><?=__('There are currently no uploads.');?></strong></p>
			</div>
		<? endif; ?>
	</div>
</div>