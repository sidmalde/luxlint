<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');

class AppController extends Controller {
	public $components = array(
		'Security' => array(
			'setHash' => 'md5',
			'validatePost' => false,
			'csrfCheck' => false,
		),
		'Acl',
		'Auth' => array(
			'authorize' => array(
				'Actions' => array('actionPath' => 'controllers')
			),
			'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => null),
			'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email'),
				),
			),
			'userScope' => array(
				'User.active' => true,
				'User.deleted' => false,
			),
			'autoRedirect' => false,
		),
		'Session',
		'Email',
		/* 'DebugKit.Toolbar', */
	);
	public $helpers = array('Html', 'Form', 'Session', 'Number', 'Time', 'Text');
	
	public $userTitles = array(
		'Mr' => 'Mr',
		'Mrs' => 'Mrs',
		'Miss' => 'Miss',
		'Dr' => 'Dr',
	);
	
	public $allowedUploadExtensions = array(
		'doc' => 'doc',
		'docx' => 'docx',
		'xls' => 'xls',
		'xlsx' => 'xlsx',
		'pdf' => 'pdf',
		'jpg' => 'jpg',
		'jpeg' => 'jpeg',
		'gif' => 'gif',
		'png' => 'png',
		'tiff' => 'tiff',
	);
	
	function beforeFilter() {
		//Configure SecurityComponent
		// Security::setHash('md5');
		
		if ($this->Auth->user()) {
			$this->currentUser = $this->Auth->user();
			$this->set('currentUser', $this->currentUser);
		}
	}
	
	function _checkAndUploadFile($basePath = 'files/', $folder, $file, $filename = null){
		
		App::import('Sanitize');
		if(!is_array($file)){
			return $file;
		} elseif($file['size']){
			if($filename){
				$file['name'] = $filename;
			} else {
				$file['name'] = basename(Sanitize::paranoid($file['name'],array('.', '-', '_')));
			}
			
			if (!file_exists($basePath.$folder)) {
				$pathToCreate = $basePath.$folder;
				mkdir($pathToCreate, 0777, true);
			}
			
			move_uploaded_file($file['tmp_name'], $basePath.$folder.'/'.$file['name']);
			return '/'.$basePath.$folder.'/'.$file['name'];
		} else {
			return NULL;
		}
	}
}
