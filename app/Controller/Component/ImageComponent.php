<?php

class ImageComponent extends Object{	
	
	// Upload a image and store in the TMP folder to then be uploaded to Amazon and removed from the TMP folder
	public function uploadLogo($file, $filename){
		if(!empty($file)){
			$filetype = $this->getFileExtension($filename);
			$filetype = strtolower($filetype);
			if ($filetype == "jpeg" || $filetype == "jpg" || $filetype == "gif" || $filetype == "png"){
				$this->crop_to_fit_img($file, $filetype, TMP.$filename, 150,80, true);    
				return TMP.$filename;
			}
		}
		return false;
	}
	
	public function upload($folder, $file, $width = 500, $height = 300, $thumbWidth = 250, $thumbHeight = 100, $square = true, $fitToSize = true){
		if(!is_array($file)){
			return $file;
		} elseif($file['size']){
			return $this->upload_image_and_thumbnail($file, $filename, array($width,$height),array($thumbWidth,$thumbHeight), $folder, $square, $fitToSize);
		} else {
			return NULL;
		}
	}
	
	/*    Parameters:
	*    $data: CakePHP data array from the form
	*    $datakey: key in the $data array. If you used <?= $form->file('Image/name1'); ?> in your view, then $datakey = name1
	*    $imgscale: the maximum width or height that you want your picture to be resized to
	*    $thumbscale: the maximum width or height that you want your thumbnail to be resized to
	*    $folderName: the name of the parent folder of the images. The images will be stored to /webroot/img/$folderName/big/ and  /webroot/img/$folderName/small/
	*    $square: a boolean flag indicating whether you want square and zoom cropped thumbnails, or thumbnails with the same aspect ratio of the source image
	*/
	private function upload_image_and_thumbnail($data, $filename, $imgscale, $thumbscale, $folderName, $square = false, $fitToSize = false) {
		if (strlen($data['name'])>4){
			$error = 0;
			$tempuploaddir = TMP; // the /temp/ directory, should delete the image after we upload
			$biguploaddir = "img/".$folderName."/big"; // the /big/ directory
			$smalluploaddir = "img/".$folderName."/small"; // the /small/ directory for thumbnails

			// Make sure the required directories exist, and create them if necessary
			if(!is_dir($tempuploaddir)) mkdir($tempuploaddir,true);
			if(!is_dir($biguploaddir)) mkdir($biguploaddir,true);
			if(!is_dir($smalluploaddir)) mkdir($smalluploaddir,true);

			$filetype = $this->getFileExtension($data['name']);
			$filetype = strtolower($filetype);

			if (($filetype != "jpeg")  && ($filetype != "jpg") && ($filetype != "gif") && ($filetype != "png")){
				return false;
			}	else	{
				$imgsize = getimagesize($data['tmp_name']);
			}

			// Generate a unique name for the image (from the timestamp)
			$id_unic = str_replace(".", "", strtotime ("now"));
			if(empty($filename)){
				$filename = $this->nameFile($data['name'], $folderName);
			}
			settype($filename,"string");
			$filename.= ".";
			$filename.=$filetype;
			$tempfile = $tempuploaddir . "/$filename";
			$resizedfile = $biguploaddir . "/$filename";
			$croppedfile = $smalluploaddir . "/$filename";

			if (is_uploaded_file($data['tmp_name'])){
				// Copy the image into the temporary directory
				if (!copy($data['tmp_name'],"$tempfile")){
					print "Error Uploading File!.";
					exit();
				}	else {
					/*
					*    Generate the big version of the image with max of $imgscale in either directions
					*/
					if($fitToSize){
						$this->crop_to_fit_img($tempfile, $resizedfile, $imgscale[0],$imgscale[1], true);                            
					} else {
						$this->resize_img($tempfile, $imgscale, $resizedfile);    
					}
					if($square) {
						/*
						*    Generate the small square version of the image with scale of $thumbscale
						*/
						$this->crop_to_fit_img($tempfile, $croppedfile, $thumbscale[0], $thumbscale[1], true);
					}	else {
						/*
						*    Generate the big version of the image with max of $imgscale in either directions
						*/
						$this->resize_img($tempfile, $thumbscale, $croppedfile);
					}
					// $this->resize_img($tempfile, $imgscale, $tempfile);   
					// Delete the temporary image
					unlink($tempfile);
				}
			}
			// Image uploaded, return the file name
			return $filename;
		}
	}
	
	/*
	*    Deletes the image and its associated thumbnail
	*    Example in controller action:    $this->Image->delete_image("1210632285.jpg","sets");
	*
	*    Parameters:
	*    $filename: The file name of the image
	*    $folderName: the name of the parent folder of the images. The images will be stored to /webroot/img/$folderName/big/ and  /webroot/img/$folderName/small/
	*/
	public function delete_image($filename,$folderName) {
		if(file_exists("img/".$folderName."/big/".$filename)){
			unlink("img/".$folderName."/big/".$filename);
			unlink("img/".$folderName."/small/".$filename);
		}
		return true;
	}
	
	private function crop_to_fit_img($src, $type, $dst, $width, $height, $crop = false){
		if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'bmp': $img = imagecreatefromwbmp($src); break;
			case 'gif': $img = imagecreatefromgif($src); break;
			case 'jpg': $img = imagecreatefromjpeg($src); break;
			case 'png': $img = imagecreatefrompng($src); break;
			default : return "Unsupported picture type!";
		}
		$y = 0;
		// resize
		if($crop){
			if($w < $width or $h < $height) return false;
			$ratio = max($width/$w, $height/$h);
			$h = $height / $ratio;
			$x = ($w - $width / $ratio) / 2;
			$w = $width / $ratio;
			//$y = ($height - $y / $ratio) / 2;
		}
		else{
			if($w < $width and $h < $height) return false;
			$ratio = min($width/$w, $height/$h);
			$width = $w * $ratio;
			$height = $h * $ratio;
			$x = 0;
		}
	
		$new = imagecreatetruecolor($width, $height);
		
		// preserve transparency
		if($type == "gif" or $type == "png"){
			imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
			imagealphablending($new, false);
			imagesavealpha($new, true);
		}
	
		imagecopyresampled($new, $img, 0, 0, $x, $y, $width, $height, $w, $h);
	
		switch($type){
			case 'bmp': imagewbmp($new, $dst); break;
			case 'gif': imagegif($new, $dst); break;
			case 'jpg': imagejpeg($new, $dst); break;
			case 'png': imagepng($new, $dst); break;
		}
		return true;
	}

	private function crop_img($imgname, $scale, $filename) {
			$filetype = $this->getFileExtension($imgname);
			$filetype = strtolower($filetype);
	
			switch($filetype){
					case "jpeg":
					case "jpg":
						$img_src = imagecreatefromjpeg ($imgname);
					 break;
					 case "gif":
						$img_src = imagecreatefromgif ($imgname);
					 break;
					 case "png":
						$img_src = imagecreatefrompng ($imgname);
					 break;
			}
	
			$width = imagesx($img_src);
			$height = imagesy($img_src);
			$ratiox = $width / $height * $scale[0];
			$ratioy = $height / $width * $scale[1];
	
			//-- Calculate resampling
			$newheight = ($width <= $height) ? $ratioy : $scale[1];
			$newwidth = ($width <= $height) ? $scale[0] : $ratiox;
	
			//-- Calculate cropping (division by zero)
			$cropx = ($newwidth - $scale[0] != 0) ? ($newwidth - $scale[0]) / 2 : 0;
			$cropy = ($newheight - $scale[1] != 0) ? ($newheight - $scale[1]) / 2 : 0;
	
			//-- Setup Resample & Crop buffers
			$resampled = imagecreatetruecolor($newwidth, $newheight);
			$cropped = imagecreatetruecolor($scale[0], $scale[1]);
	
			//-- Resample
			if($filetype == 'png' || $filetype == 'gif'){
				imagealphablending($resampled, false);
				imagesavealpha($resampled,true);
				$transparent = imagecolorallocatealpha($resampled, 255, 255, 255, 127);
				imagefilledrectangle($resampled, 0, 0, $newwidth, $newheight, $transparent);
				imagecopyresampled($resampled, $img_src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				// Save the cropped image
				switch($filetype){
					 case "gif":
					 imagegif($resampled,$filename,100);
					 break;
					 case "png":
					 imagepng($resampled,$filename,9);
					 break;
				}
			} else {
				imagecopyresampled($resampled, $img_src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				imagecopy($cropped, $resampled, 0, 0, $cropx, $cropy, $newwidth, $newheight);
				// Save the cropped image
				switch($filetype){
					case "jpeg":
					case "jpg":
					 imagejpeg($cropped,$filename,100);
					 break;
				}
			}
	}
	
	private function resize_img($imgname, $size, $filename)    {
			$filetype = $this->getFileExtension($imgname);
			$filetype = strtolower($filetype);
	
			switch($filetype) {
					case "jpeg":
					case "jpg":
					$img_src = ImageCreateFromjpeg ($imgname);
					break;
					case "gif":
					$img_src = imagecreatefromgif ($imgname);
					break;
					case "png":
					$img_src = imagecreatefrompng ($imgname);
					break;
			}
	
			$true_width = imagesx($img_src);
			$true_height = imagesy($img_src);
			//echo "Mex width = ".$size[0]."<br />";
			//echo "Mex height = ".$size[1]."<br />";
			//echo "True width = ".$true_width."<br />";
			//echo "True height = ".$true_height."<br />";
			
			// now we check for over-sized images and pare them down
			// to the dimensions we need for display purposes
			$ratio =  ( $true_width > $size[0] ) ? (real)($size[0] / $true_width) : 1 ;
			$width = ((int)($true_width * $ratio));    //full-size width
			$height = ((int)($true_height * $ratio));    //full-size height
			
			//echo "Ratio = ".$ratio."<br />";
			//echo "New width = ".$width."<br />";
			//echo "New height = ".$height."<br />";
			
			//check for images that are still too high
			$ratio =  ( $height > $size[1] ) ? (real)($size[1] / $height) : 1 ;
			$width = ((int)($width * $ratio));    //mid-size width
			$height = ((int)($height * $ratio));    //mid-size height
			
			//echo "Ratio = ".$ratio."<br />";
			//echo "New width = ".$width."<br />";
			//echo "New height = ".$height."<br />";
			//die();
			$resampled = imagecreatetruecolor($width,$height);
			if($filetype == 'png' || $filetype == 'gif'){
				imagealphablending($resampled, false);
				imagesavealpha($resampled,true);
				$transparent = imagecolorallocatealpha($resampled, 255, 255, 255, 127);
				imagefilledrectangle($resampled, 0, 0, $width, $height, $transparent);
				imagecopyresampled($resampled, $img_src, 0, 0, 0, 0, $width, $height, $true_width, $true_height);
			} else {
				imagecopyresampled ($resampled, $img_src, 0, 0, 0, 0, $width, $height, $true_width, $true_height);
			}
			// Save the resized image
			switch($filetype){
				case "jpeg":
				case "jpg":
				 imagejpeg($resampled,$filename,100);
				 break;
				 case "gif":
				 imagegif($resampled,$filename,100);
				 break;
				 case "png":
				 imagepng($resampled,$filename,9);
				 break;
			}
	}
	
	private function getFileExtension($str) {
			$i = strrpos($str,".");
			if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
			return $ext;
	}
	
	private function nameFile($str, $foldername) {
			$i = strrpos($str,".");
			if (!$i) { return ""; }
			$str = str_replace(" ", "_", $str);
			$fileName = substr($str,0,$i);
			if(file_exists("img/".$foldername."/big/".$str)){
				$fileName = rand(0,99999).'_'.$fileName;
			}
			return $fileName;
	}
}