<?php
App::uses('AppController', 'Controller');

class ProductsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'products');
		$this->Auth->allow();
		$this->layout = 'admin';
	}
	
	public function admin_index() {
		
	}
	
	public function admin_view() {
		
	}
	
	public function admin_add() {
		
	}
	
	public function admin_edit() {
		
	}
	
	public function admin_delete() {
		
	}
}
